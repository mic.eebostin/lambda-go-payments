.PHONY: build clean deploy

build:
	env GOARCH=amd64 GOOS=linux go build -ldflags="-s -w" -o bin/create src/payment/create/main.go

clean:
	rm -rf ./bin

test: 
	go test ./src/payment/create

apply: clean build
	cd terraform && terraform apply --auto-approve 

destroy:
	cd terraform && terraform destroy --auto-approve 

setup: build
	cp dist.env.json build/env.json 

develop:
	sam local start-api -p 3001 --env-vars build/env.json