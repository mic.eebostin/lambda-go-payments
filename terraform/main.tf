terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.48.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.1.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "~> 2.2.0"
    }
  }

  required_version = "~> 1.0"
}

provider "aws" {
  region = var.aws_region
}


# IAM roles

resource "aws_iam_role" "lambda_exec" {
  name = "lambda_payment_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_exec.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

# set s3 bucket

resource "random_pet" "bucket_name" {
  prefix = "lambda-payment"
  length = 4
}

resource "aws_s3_bucket" "bucket_s3" {
  bucket = random_pet.bucket_name.id

  acl           = "private"
  force_destroy = true
}

# prepare zip file to upload

data "archive_file" "payment_zip" {
  type = "zip"

  source_dir  = "${path.module}/../bin"
  output_path = "${path.module}/../build/create.zip"
}

resource "aws_s3_bucket_object" "payment_s3_object" {
  bucket = aws_s3_bucket.bucket_s3.id

  key    = "create.zip"
  source = data.archive_file.payment_zip.output_path

  etag = filemd5(data.archive_file.payment_zip.output_path)
}

# secrets

data "aws_secretsmanager_secret_version" "api_secrets" {
  secret_id = "payment-api-secrets"
}

locals {
  api_creds = jsondecode(
    data.aws_secretsmanager_secret_version.api_secrets.secret_string
  )
}

# configure lambda

resource "aws_lambda_function" "payment_function" {
  function_name = "create_payment_function"

  s3_bucket = aws_s3_bucket.bucket_s3.id
  s3_key    = aws_s3_bucket_object.payment_s3_object.key

  runtime = "go1.x"
  handler = "create"

  source_code_hash = data.archive_file.payment_zip.output_base64sha256

  role = aws_iam_role.lambda_exec.arn

  environment {
    variables = {
      API_KEY = local.api_creds.API_KEY,
      API_URL = local.api_creds.API_URL
    }
  }
}

# set api gateway

resource "aws_apigatewayv2_api" "payment_api" {
  name          = "create_payment_api"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "payment_api_stage" {
  api_id = aws_apigatewayv2_api.payment_api.id

  name        = "$default"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.payment_api_logs.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "payment_api_integration" {
  api_id = aws_apigatewayv2_api.payment_api.id

  integration_uri    = aws_lambda_function.payment_function.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "payment_api_route" {
  api_id = aws_apigatewayv2_api.payment_api.id

  route_key = "POST /create"
  target    = "integrations/${aws_apigatewayv2_integration.payment_api_integration.id}"
}


resource "aws_lambda_permission" "payment_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.payment_function.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.payment_api.execution_arn}/*/*"
}


# enable logs

resource "aws_cloudwatch_log_group" "payment_lambda_logs" {
  name = "/aws/lambda/${aws_lambda_function.payment_function.function_name}"

  retention_in_days = 1
}

resource "aws_cloudwatch_log_group" "payment_api_logs" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.payment_api.name}"

  retention_in_days = 1
}