package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type Response events.APIGatewayProxyResponse

type CreatePaymentEvent struct {
	Source    string `json:"source"`
	Reference string `json:"reference"`
	Amount    int    `json:"amount"`
}

type PaymentRequest struct {
	Reference string `json:"reference"`
	Amount    int    `json:"amount"`
}

type APIPaymentConfiguration struct {
	Key string
	URL string
}

func ValidatePayment(event CreatePaymentEvent) (bool, error) {
	fmt.Print(event)

	if event.Amount <= 0 {
		return false, errors.New("amount must be greater then 0")
	}

	return true, nil
}

func SendPaymentRequest(event CreatePaymentEvent, config APIPaymentConfiguration) (bool, error) {
	paymentRequest := &PaymentRequest{Reference: event.Reference, Amount: event.Amount}
	paymentRequestJson, err := json.Marshal(paymentRequest)

	if err != nil {
		return false, err
	}

	reader := bytes.NewReader(paymentRequestJson)
	request, err := http.NewRequest("POST", config.URL, reader)

	if err != nil {
		return false, err
	}

	request.Header.Set("X-Auth-Signed", config.Key)

	client := &http.Client{}
	res, err := client.Do(request)

	if err != nil {
		return false, err
	}

	if res.StatusCode != 200 {
		return false, nil
	}

	return true, nil
}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (Response, error) {
	event := CreatePaymentEvent{}
	json.Unmarshal([]byte(req.Body), &event)

	isValidPayment, err := ValidatePayment(event)

	if !isValidPayment {
		return Failure(400), nil
	}

	if err != nil {
		return Failure(400), nil
	}

	apiConfiguration := APIPaymentConfiguration{Key: os.Getenv("API_KEY"), URL: os.Getenv("API_URL")}
	isPaymentSent, err := SendPaymentRequest(event, apiConfiguration)

	if !isPaymentSent {
		return Failure(503), nil
	}

	if err != nil {
		return Failure(503), nil
	}

	resp := Response{
		StatusCode:      201,
		IsBase64Encoded: false,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}

	return resp, nil
}

func Failure(statusCode int) Response {
	resp := Response{
		StatusCode:      statusCode,
		IsBase64Encoded: false,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
	}

	return resp
}

func main() {
	lambda.Start(Handler)
}
