package main

import (
	"testing"
)

func TestValidatePayment(t *testing.T) {

	createPayment := CreatePaymentEvent{
		Amount:    0,
		Reference: "ref",
		Source:    "source",
	}

	t.Run("error when amount is not greater then 0", func(t *testing.T) {
		got, err := ValidatePayment(createPayment)
		want := "amount must be greater then 0"
		assertFalse(t, got)
		assertCorrectMessage(t, err.Error(), want)
	})

	createPayment.Amount = 100

	t.Run("payment created", func(t *testing.T) {
		got, err := ValidatePayment(createPayment)
		assertTrue(t, got)
		assertNil(t, err)
	})

}

func assertNil(t testing.TB, got error) {
	t.Helper()
	if got != nil {
		t.Errorf("expected nil got %d", got)
	}
}

func assertFalse(t testing.TB, got bool) {
	t.Helper()
	if got != false {
		t.Errorf("expected false")
	}
}

func assertTrue(t testing.TB, got bool) {
	t.Helper()
	if got != true {
		t.Errorf("expected true")
	}
}

func assertCorrectMessage(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
