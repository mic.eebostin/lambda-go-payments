module create

go 1.17

require (
	github.com/aws/aws-lambda-go v1.28.0 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
	golang.org/x/tools v0.1.9 // indirect
)
